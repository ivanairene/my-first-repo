from django.conf.urls import url
from .views import index, add_todo, delete_entry

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^delete_entry/(?P<id>\w+)/$', delete_entry, name='delete_entry'),
]