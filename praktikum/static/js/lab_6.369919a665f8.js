// ChatBox
function sendChat(e) {
  var id = e.target.id;
  var notEmpty = $(".message-input").val() != "",
  isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
  isSendClick = e.type == "click" && id == "send";

  if( notEmpty && (isEnterKeypress || isSendClick) ) {
    var txt = $(".message-input").val();
    var rply = ["oi",
    "hah?",
    "anjer",
    "sipsipp",
    "oyyy",
    "hmmmm",
    "ga jelas lu",
    "slo",
    ]
    var min = 0;
    var max = 7;
    var random = Math.floor(Math.random() * (max - min + 1)) + min; 
    $('<p>'+txt+'</p>').addClass('msg-send').appendTo('.msg-insert'); 
    $(".message-input").val("");
    $('<p>'+rply[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert'); 
    $(".chat-body").scrollTop($(".chat-body").height());
  }
}
$(".chat-text").keypress(sendChat);

// Calculator
var print = document.getElementById('print');
var erase = false;

function go(x) {
  if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

function changeTheme(theme) {
  console.log('theme', theme)
  $('body').css('background-color',theme.bcgColor);
  // $('body').css('color',theme.fontColor);
}
// Store
localStorage.setItem("themes", JSON.stringify([
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
])
);

var daftarTheme = {
  0 : {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  1 : {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  2 : {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  3 : {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  4 : {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  5 : {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  6 : {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  7 : {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  8 : {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  9 : {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  10 : {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"},
}
var themeNow = {"Indigo": {"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
if (!localStorage.getItem("selectedTheme")) {
  localStorage.setItem("selectedTheme", JSON.stringify(themeNow['Indigo']))
}
changeTheme(JSON.parse(localStorage.getItem("selectedTheme")))

$(document).ready(function() {
    $('.my-select').select2({
      'data': JSON.parse(localStorage.getItem("themes"))
    });
});

$('.apply-button').on('click', function() {  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $('.my-select').val()
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    var objTheme = daftarTheme[id];
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    changeTheme(objTheme);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("selectedTheme", JSON.stringify(objTheme));
})
